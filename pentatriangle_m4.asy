import feynman;

int l = 40, // polygon side length
		n1 = 3,
		n2 = 5;

currentpen = linewidth(0.5);
pen massive = linewidth(3);
arrowbar arrow = Arrow(TeXHead, 1);
transform ls = scale(1);

real outer_radius(int l, int n) {
	return l / 2 / Sin(360 / n / 2);
}

real inner_radius(int l, int n) {
	return l / 2 / Tan(360 / n / 2);
}

pair polygon_vertex(int i, int n, int r = 0) {
	return dir(i * 360 / n + r);
}

pair[] polygon_vertices(int l, int n, int r = 0) {
	return sequence(new pair(int i) { return outer_radius(l, n) * polygon_vertex(i, n, r); }, n);
}

pair ext(pair[] p, int i, int l, int n, int r = 0) {
	return p[i] + outer_radius(l, n) * polygon_vertex(i, n, r);
}

pair[] t = polygon_vertices(l, n1, 60) - inner_radius(l, n1) - inner_radius(l, n2),
	p = polygon_vertices(l, n2);

pair c0 = ext(p, 1, l, n2),
	   c1 = ext(p, 0, l, n2),
		 c2 = ext(p, 4, l, n2),
		 c3 = ext(t, 1, l, n1, 60);

path l1 = p[1]--c0,
     l2 = p[0]--c1,
     l3 = p[4]--c2,
     l4 = t[1]--c3;

draw(l1);
draw(l2);
draw(l3);
draw(l4, massive);

path penta = p[0]--p[1]--p[2]--p[3]--p[4]--cycle,
     tri = t[0]--t[1]--t[2]--cycle;
fill(penta, white);
fill(tri, white);

draw(tri);
draw(penta);

label("$1$", c0, polygon_vertex(1, n2));
label("$2$", c1, E);
label("$3$", c2, polygon_vertex(4, n2));
label("$4$", c3, W);

// drawMomArrow(l1, RightSide, arrow);
// label(ls*Label("$p_1$", 2*RightSide), l1);

// drawMomArrow(l2, RightSide, arrow);
// label(ls*Label("$p_2$", 2*RightSide), l2);

// drawMomArrow(l3, LeftSide, arrow);
// label(ls*Label("$p_3$", 2*LeftSide), l3);

// drawMomArrow(l4, LeftSide, arrow);
// label(ls*Label("$p_4$", 2*LeftSide), l4);

real m = 2.5;
path pp = p[2]--p[1];
drawMomArrow(pp, LeftSide, arrow);
label(ls*Label("$k_1$", m*LeftSide), pp);

path tt = t[0]--t[1];
drawMomArrow(tt, RightSide, arrow);
label(ls*Label("$k_2$", m*RightSide), tt);

import feynman;

int l = 30;

pair i1 = (-sqrt(3)/2*l, l/2),
		 i2 = (-sqrt(3)/2*l, -l/2),
		 c = (0, 0),
		 t = (0, l),
		 b = (0, -l),
		 o1 = (l, l/2),
		 o2 = (l, -l/2);

fill(circle(c, 1.2*l), white);

draw(t--o1);
draw(t--o2);

draw(c--o1);
draw(c--o2);

draw(b--o1);
draw(b--o2);

drawGluon(arc(c, i1, t, CW), erasebg=false);
drawGluon(arc(c, b, i2, CW), erasebg=false);

drawFermion(arc(i2, i1, c, CW), erasebg=false);
drawFermion(arc(i1, c, i2, CW), erasebg=false);
drawFermion(arc(c, i2, i1, CW), erasebg=false);

drawVertex(t);
drawVertex(c);
drawVertex(b);

drawVertex(o1);
drawVertex(o2);

drawVertex(i1);
drawVertex(i2);

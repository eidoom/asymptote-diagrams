import feynman;

int l = 40;

pair[] a = sequence(new pair(int i){return (l * i, 0);}, 3),
	b = sequence(new pair(int i){return (l * i, -l);}, 3);

pen massive = linewidth(3);
pen eraser = white+linewidth(6);
arrowbar arrow = Arrow(TeXHead, 1);
transform ls = scale(1);

pair c1 = a[2]+l/sqrt(2)*dir(45),
     c2 = b[2]+l/sqrt(2)*dir(315),
     c3 = b[0]+l/sqrt(2)*dir(225),
     c4 = a[0]+l/sqrt(2)*dir(135);

path l1 = a[2]--c1,
     l2 = b[2]--c2,
     l3 = b[0]--c3,
     l4 = a[0]--c4;

draw(l4, massive);

fill(a[0]--a[2]--b[2]--b[0]--cycle, white);

draw(a[0]--b[1]);
path c = a[1]--b[0];
draw(c, eraser);
draw(c);
draw(a[2]--b[2]);

draw(a[0]--a[2]);
draw(b[0]--b[2]);

draw(l1);
draw(l2);
draw(l3);

// drawMomArrow(l1, RightSide, arrow);
// label(ls*Label("$p_1$", 2*RightSide), l1);

// drawMomArrow(l2, LeftSide, arrow);
// label(ls*Label("$p_2$", 2*LeftSide), l2);

// drawMomArrow(l3, RightSide, arrow);
// label(ls*Label("$p_3$", 2*RightSide), l3);

// drawMomArrow(l4, LeftSide, arrow);
// label(ls*Label("$p_4$", 2*LeftSide), l4);

real m = 2.5;
path k1 = a[1]--a[2];
drawMomArrow(k1, LeftSide, arrow);
label(ls*Label("$k_1$", m*LeftSide), k1);

path k2 = a[1]--l/2*(1,-1);
drawMomArrow(k2, LeftSide, arrow);
label(ls*Label("$k_2$", m*LeftSide), k2);

label("$1$", c1, NE);
label("$2$", c2, SE);
label("$3$", c3, SW);
label("$4$", c4, NW);

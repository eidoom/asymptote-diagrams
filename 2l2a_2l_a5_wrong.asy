import feynman;

int l = 40;

real d = l/sqrt(2);

pair a0 = (0, 0),
		 a1 = (0, -l),
		 a2 = (l, -l),
		 a3 = (l, 0),
		 e0 = (0, -l/3),
		 e1 = (-l/6, -l/2),
		 e2 = (0, -2*l/3),
		 e3 = (l/6, -l/2),
		 c0 = a0+d*dir(135),
		 c1 = a1+d*dir(225),
		 c2 = a2+d*dir(315),
		 c3 = a3+d*dir(45),
		 b0 = a3+d/3*dir(45),
		 b2 = b0+d/3*dir(45),
		 d0 = a3+d/2*dir(45),
		 b1 = d0+d/6*dir(135),
		 b3 = d0+d/6*dir(315);

currentpen = linewidth(0.5);
pen massive = linewidth(3);

drawFermion(c0--a0--a3--a2--a1--c1);

drawPhoton(a2--c2);

drawPhoton(a3--b0, massive, erasebg=false);
drawPhoton(b2--c3, massive, erasebg=false);

drawFermion(b3..b0..b1..b2..cycle, erasebg=false);

drawPhoton(a0--e0, erasebg=false);
drawPhoton(e2--a1, erasebg=false);

drawFermion(e1..e0..e3..e2..cycle, erasebg=false);

label("$1_\ell$", c0, NW);
label("$2_{\bar\ell}$", c1, SW);
label("$3_\gamma$", c2, SE);
label("$4_{\gamma^*}$", c3, NE);

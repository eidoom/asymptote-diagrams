SRC=$(wildcard *.asy)
PDF=$(SRC:asy=pdf)

.PHONY: all intersect clean

all: $(PDF) intersect finn_logo.png

intersect: intersect.pdf intersect-hep.pdf intersect-ml.pdf intersect-menns.pdf

intersect.pdf: intersect.asy.template
	cp $< $(basename $@).asy
	sed -i 's/FILL1/gray(0.3)/' $(basename $@).asy
	sed -i 's/FILL2/gray(0.3)/' $(basename $@).asy
	sed -i 's/FILL3/gray(0.3)/' $(basename $@).asy
	sed -i 's/TEXT1/black/' $(basename $@).asy
	sed -i 's/TEXT2/black/' $(basename $@).asy
	sed -i 's/TEXT3/black/' $(basename $@).asy
	asy -f pdf $(basename $@).asy
	rm $(basename $@).asy

intersect-hep.pdf: intersect.asy.template
	cp $< $(basename $@).asy
	sed -i 's/FILL1/white/' $(basename $@).asy
	sed -i 's/FILL2/gray(0.3)/' $(basename $@).asy
	sed -i 's/FILL3/white/' $(basename $@).asy
	sed -i 's/TEXT1/gray(0.5)/' $(basename $@).asy
	sed -i 's/TEXT2/gray(0.5)/' $(basename $@).asy
	sed -i 's/TEXT3/black/' $(basename $@).asy
	asy -f pdf $(basename $@).asy
	rm $(basename $@).asy

intersect-ml.pdf: intersect.asy.template
	cp $< $(basename $@).asy
	sed -i 's/FILL1/gray(0.3)/' $(basename $@).asy
	sed -i 's/FILL2/white/' $(basename $@).asy
	sed -i 's/FILL3/white/' $(basename $@).asy
	sed -i 's/TEXT1/black/' $(basename $@).asy
	sed -i 's/TEXT2/gray(0.5)/' $(basename $@).asy
	sed -i 's/TEXT3/gray(0.5)/' $(basename $@).asy
	asy -f pdf $(basename $@).asy
	rm $(basename $@).asy

intersect-menns.pdf: intersect.asy.template
	cp $< $(basename $@).asy
	sed -i 's/FILL1/white/' $(basename $@).asy
	sed -i 's/FILL2/white/' $(basename $@).asy
	sed -i 's/FILL3/gray(0.3)/' $(basename $@).asy
	sed -i 's/TEXT1/gray(0.5)/' $(basename $@).asy
	sed -i 's/TEXT2/black/' $(basename $@).asy
	sed -i 's/TEXT3/gray(0.5)/' $(basename $@).asy
	asy -f pdf $(basename $@).asy
	rm $(basename $@).asy

$(PDF): %.pdf: %.asy
	asy -f pdf $<

finn_logo.png: finn_logo.pdf
	convert -density 300 -quality 100 $< $@

clean:
	-rm *.pdf

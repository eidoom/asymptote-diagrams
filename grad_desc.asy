import graph;

size(10cm,0);

real a = 0.2,
		 b = 0.9,
		 c = 1,
		 d = 0.6,
		 e = 1.1;

real f(real x) {
	return d*(a-x)^2+b*(c-x^2)^2;
}

real ff(real x) {
	return -2*d*(a-x)-4*b*x*(c-x^2);
}

draw(graph(f, -e, e));

real x0 = 0,
		 g = -inf;

dot((x0, f(x0)), red);

while (g < -0.1) {
	g = ff(x0);
	x0 -= 0.15 * g;
	dot((x0, f(x0)), red);
}

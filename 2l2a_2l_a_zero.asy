import feynman;

int l = 40, // polygon side length
		n1 = 3,
		n2 = 5;

currentpen = linewidth(0.5);
pen massive = linewidth(3);

real outer_radius(int l, int n) {
	return l / 2 / Sin(360 / n / 2);
}

real inner_radius(int l, int n) {
	return l / 2 / Tan(360 / n / 2);
}

pair polygon_vertex(int i, int n, int r = 0) {
	return dir(i * 360 / n + r);
}

pair[] polygon_vertices(int l, int n, int r = 0) {
	return sequence(new pair(int i) { return outer_radius(l, n) * polygon_vertex(i, n, r); }, n);
}

pair[] t = polygon_vertices(l, n1, 60) - inner_radius(l, n1) - inner_radius(l, n2),
	p = polygon_vertices(l, n2);

drawFermion(t[0]--t[1]--t[2]--t[0]);

pair ext(pair[] p, int i, int l, int n, int r = 0) {
	return p[i] + outer_radius(l, n) * polygon_vertex(i, n, r);
}

pair c0 = ext(p, 1, l, n2),
c1 = ext(p, 4, l, n2),
c2 = ext(p, 0, l, n2),
c3 = ext(t, 1, l, n1, 60);

drawFermion(c1--p[4]--p[0]--p[1]);
draw(p[1]--c0);

drawPhoton(p[3]--p[4]);
drawPhoton(p[1]--p[2]);

drawPhoton(p[0]--c2);

drawPhoton(t[1]--c3, erasebg=false, massive);

label("$1_\ell$", c0, E);
label("$2_{\bar\ell}$", c1, E);
label("$3_\gamma$", c2, E);
label("$4_{\gamma^*}$", c3, W);

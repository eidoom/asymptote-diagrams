import feynman;

int l = 30,
		n = 5;

real r = l / 2 / Sin(360/n/2);

pair poly(int i) {
	return dir(i*360/n);
}

pair[] p = sequence(new pair(int i){return r*poly(i);}, n),
	a = sequence(new pair(int i){return p[2] - ((i+1)*l,0);}, 2),
	b = sequence(new pair(int i){return p[3] - ((i+1)*l,0);}, 2);

pair ext(int i) {
	return p[i] + l*poly(i);
}

draw(a[1]--a[0]--p[2]);
drawFermion(p[2]--p[1]);
draw(p[1]--p[0]--p[4]);
drawFermion(p[4]--p[3]);
draw(p[3]--b[0]--b[1]);

drawGluon(a[0]--b[0]);
drawGluon(p[2]--p[3]);

for(int i=1; i!=3; i=(i - 1) % n) {
	drawGluon(p[i]--ext(i));
}

for(pair v : p) {
	drawVertex(v);
}
drawVertex(a[0]);
drawVertex(b[0]);

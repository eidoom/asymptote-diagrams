size(300);

real e = 1;

pen op = opacity(1, "Difference");

path square(pair pos, real l) {
	return shift(pos)*scale(l)*unitsquare;
}

fill(square((-e, -e), 3*e), black+op);

fill(circle((e/2, e), e), red+op);
fill(circle((e, 0), e), green+op);
fill(circle((0, 0), e), blue+op);

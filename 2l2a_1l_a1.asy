import feynman;

int l = 40;

pair a0 = (0, 0),
		 a1 = (0, -l),
		 a2 = (l, -l),
		 a3 = (l, 0),
		 c0 = a0+l/sqrt(2)*dir(135),
		 c1 = a1+l/sqrt(2)*dir(225),
		 c2 = a2+l/sqrt(2)*dir(315),
		 c3 = a3+l/sqrt(2)*dir(45);

currentpen = linewidth(0.5);
pen massive = linewidth(3);
arrowbar arrow = MidArrow(TeXHead, 1.5);

drawPhoton(a2--c2);
drawPhoton(a3--c3, massive);

fill(a0--a3--a2--a1--cycle, white);
drawFermion(c0--a0--a3--a2--a1--c1, arrow, erasebg=false);
drawPhoton(a0--a1);

label("$1_\ell$", c0, NW);
label("$2_{\bar\ell}$", c1, SW);
label("$3_\gamma$", c2, SE);
label("$4_{\gamma^*}$", c3, NE);

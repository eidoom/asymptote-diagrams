# [asymptote-diagrams](https://gitlab.com/eidoom/asymptote-diagrams)
## Links for Asymptote
* [Asymptote homepage](https://asymptote.sourceforge.io/)
* [Language reference](https://asymptote.sourceforge.io/doc/Index.html)
* [Source code](https://github.com/vectorgraphics/asymptote)

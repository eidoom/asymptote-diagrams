import feynman;

int l = 40;

pair a0 = (-l, l/2),
		 a1 = (0, l/2),
		 a2 = (l, l/2),
		 d = a1+l/2,
		 c0 = d-l/6,
		 c1 = d+(0,l/6),
		 c2 = d+l/6,
		 c3 = d-(0,l/6),
		 b0 = (-l, -l/2),
		 b1 = (0, -l/2),
		 b2 = (l, -l/2);

currentpen = linewidth(0.5);
pen massive = linewidth(3);
arrowbar arrow = MidArrow(TeXHead, 1.5);

drawFermion(a0--a1--b1--b0, arrow);

drawPhoton(a1--c0, erasebg=false);
drawPhoton(c2--a2, massive, erasebg=false);
drawPhoton(b1--b2);

path loop = c3..c0..c1..c2..cycle;
fill(loop, white);
drawFermion(loop, arrow, erasebg=false);

label("$1_\ell$", a0, W);
label("$2_{\bar\ell}$", b0, W);
label("$3_\gamma$", b2, E);
label("$4_{\gamma^*}$", a2, E);

import feynman;

int l = 40;

pair a0 = (-l, l/2),
		 a1 = (0, l/2),
		 d = a1+l/2,
		 c0 = d-l/6,
		 c1 = d+(0,l/6),
		 c2 = d+l/6,
		 c3 = d-(0,l/6),
		 e = d+2*l/3,
		 f0 = e-l/6,
		 f1 = e+(0,l/6),
		 f2 = e+l/6,
		 f3 = e-(0,l/6),
		 a3 = e+l/2,
		 b0 = (-l, -l/2),
		 b1 = (0, -l/2),
		 b2 = (l, -l/2),
		 b3 = b2 + 2*l/3;

currentpen = linewidth(0.5);
pen massive = linewidth(3);
arrowbar arrow = MidArrow(TeXHead, 1.5);

drawFermion(a0--a1--b1--b0, arrow);

drawPhoton(a1--c0, erasebg=false);
drawPhoton(c2--f0, erasebg=false);
drawPhoton(f2--a3, massive, erasebg=false);
drawPhoton(b1--b3);

drawFermion(c3..c0..c1..c2..cycle, erasebg=false, arrow);
path loop = f3..f0..f1..f2..cycle;
fill(loop, white);
drawFermion(loop, erasebg=false, arrow);

label("$1_\ell$", a0, W);
label("$2_{\bar\ell}$", b0, W);
label("$3_\gamma$", b3, E);
label("$4_{\gamma^*}$", a3, E);

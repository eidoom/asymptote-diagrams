import feynman;

int l = 30;

pair[] a = sequence(new pair(int i){return (l*i,0);}, 5),
	b = sequence(new pair(int i){return (l*i,-l);}, 5);

pair c = l*(2,-0.5),
		 d = l*(2.5,-0.5);

drawGluon(a[0]--a[1]);
drawGluon(b[0]--b[1]);

drawGluon(a[2]--c, erasebg=false);
drawGluon(c--d, erasebg=false);
drawGluon(c--b[2], erasebg=false);

drawPhoton(a[3]--a[4]);
drawPhoton(b[3]--b[4]);

drawFermion(a[1]--a[2]);
draw(a[2]--a[3]--b[3]);
drawFermion(b[3]--b[2]);
draw(b[2]--b[1]--a[1]);

for(pair v : a[1:a.length-1]) {
	drawVertex(v);
}
for(pair v : b[1:b.length-1]) {
	drawVertex(v);
}
drawVertex(c);

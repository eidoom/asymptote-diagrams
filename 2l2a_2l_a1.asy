import feynman;

int l = 40;

pair[] a = sequence(new pair(int i){return (l * i, 0);}, 3),
	b = sequence(new pair(int i){return (l * i, -l);}, 3);

pair c0 = a[0]+l/sqrt(2)*dir(135),
		 c1 = b[0]+l/sqrt(2)*dir(225),
		 c2 = b[2]+l/sqrt(2)*dir(315),
		 c3 = a[2]+l/sqrt(2)*dir(45);

currentpen = linewidth(0.5);
pen massive = linewidth(3);
arrowbar arrow = MidArrow(TeXHead, 1.5);

drawPhoton(b[2]--c2, erasebg=false);
drawPhoton(a[2]--c3, erasebg=false, massive);

fill(a[0]--a[2]--b[2]--b[0]--cycle, white);
drawFermion(c0--a[0]--a[1]--a[2]--b[2]--b[1]--b[0]--c1, arrow, erasebg=false);

drawPhoton(a[0]--b[0]);
drawPhoton(a[1]--b[1]);

label("$1_\ell$", c0, NW);
label("$2_{\bar\ell}$", c1, SW);
label("$3_\gamma$", c2, SE);
label("$4_{\gamma^*}$", c3, NE);
